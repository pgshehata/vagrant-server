#!/usr/bin/env bash

#####################################################################################
#																					#
#   SCRIPT de mise en place d'un environnement Vagrant sous debian8 et Virtual Box	#
#																					#
##############################test sur Val de Marne##################################


# connection en root
sudo -i
source /vagrant/provisioning/scripts/variables_env.sh

# Récupération des packages	
	# mise à jour de apt-get et ajout du dépôt pour nodejs	
	sudo apt-get -y update	
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
	
	# installation d'apache
	apt-get install -y apache2 
	if [ ! -d /var/www/html/mydomain ]; then
	  mkdir /var/www/mydomain
	fi	

	#  installation de mysql
	debconf-set-selections <<< "mysql-server mysql-server/root_password password ${MYSQL_PASS}"
	debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${MYSQL_PASS}"
	sudo apt-get install -y  mysql-client mysql-server

	# installation de PHP
	sudo apt-get install -y php5 libapache2-mod-php5
	sudo apt-get install -y php5-common php5-dev php5-cli php5-fpm 
	sudo apt-get install -y curl php5-curl php5-gd php5-mcrypt php5-mysql mcrypt

	# Install composer
	curl -s https://getcomposer.org/installer | php
	sudo mv composer.phar /usr/local/bin/composer
	sudo ln -s /usr/local/bin/composer /usr/bin/composer
	sudo chmod 777 /usr/local/bin/composer
	export PATH="$HOME/.composer/vendor/bin:$PATH"
	
	#install drupal console
	curl https://drupalconsole.com/installer -L -o drupal.phar
	sudo mv drupal.phar /usr/local/bin/drupal
	sudo chown root:staff /usr/local/bin/composer
	sudo chown root:staff /usr/local/bin/drupal	
	sudo chmod 777 /usr/local/bin/drupal
	drupal self-update
	
	sudo apt-get install -y nano
	sudo apt-get install -y tree
	sudo apt-get install -y wget

	# CONFIGURATION de phpmyadmin
	debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password ${MYSQL_PASS}"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password ${MYSQL_PASS}"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password ${MYSQL_PASS}"
	debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
	sudo apt-get -y install phpmyadmin	
	
	# A exécuter en tant qu'Admin
	sudo ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin

	# Install Git
	sudo apt-get install git -y
	
	# Install drush
	sudo git clone https://github.com/drush-ops/drush.git /usr/local/src/drush
	cd /usr/local/src/drush
	sudo git checkout 8.1.12
	sudo ln -s /usr/local/src/drush/drush /usr/bin/drush
	sudo composer install
	


	# Mise en place du VHost

	# touch /etc/apache2/sites-available/${PROJECTNAME}.conf
	VHOST=$(cat <<EOF
<VirtualHost *:80>
	DocumentRoot "/var/www/html/mydomain"
	ServerName mydomain.local
	
	<Directory "/var/www/html/mydomain">
		Options Indexes FollowSymLinks
		AllowOverride All
		Order allow,deny
		Allow from all
		Require all granted
	</Directory>
	
	ErrorLog "/var/log/apache2/mydomain.err.local.log"
	CustomLog "/var/log/apache2/mydomain.local.log" common
</VirtualHost>
EOF
)
[ -f "/etc/apache2/sites-available/mydomain.conf" ] && echo "mydomain.conf exist." || sudo echo "${VHOST}" >> "/etc/apache2/sites-available/mydomain.conf"

	#config.vm.provision "shell", path: "./mailcatcher.sh", privileged: true
	
	# MODULES et BDD
	# Activation des modules apache
	sudo mkdir /etc/apache2/logs
	sudo a2enmod rewrite
	sudo a2enmod php5
	sudo php5enmod mcrypt

	# Activation du site
	a2dissite 000-default.conf
	sudo service apache2 reload
	a2ensite mydomain.conf
	sudo service apache2 reload


	# Création de la BDD
	#mysql -u root -proot -e "CREATE DATABASE ${DBNAME} CHARACTER SET utf8 COLLATE utf8_general_ci";	

	# Import de la basevb
	#mysql -u root -proot mydomain_dev < /vagrant/provisioning/databases/201711031533_dev.sql



	# redémarrage d'apache pour charger les changements
	sudo systemctl restart apache2
	

